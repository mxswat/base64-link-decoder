# Base64 link decoder

This Google Chrome extension enables you to decode the selected text and open a new tab with it via right click (the context menu)

Extensions can be loaded in unpacked mode by following the following steps:

* Download this extension from https://bitbucket.org/mxswat/base64-link-decoder/downloads/
* Extract the content of the zip where you want
* Visit chrome://extensions (via omnibox or menu -> Tools -> Extensions).
* Enable Developer mode by ticking the checkbox in the upper-right corner.
* Click on the "Load unpacked extension..." button.
* Select the directory containing the unpacked extension.