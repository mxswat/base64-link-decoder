'use strict';

chrome.runtime.onInstalled.addListener(function () {
  chrome.contextMenus.create({
    id: 'mxBase64Decode',
    title: 'Decode Link',
    type: 'normal',
    contexts: ['selection'],
  });
});

// Credit to Mark Redman https://stackoverflow.com/questions/1701898/how-to-detect-whether-a-string-is-in-url-format-using-javascript/1701911
function isUrl(s) {
  var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
  return regexp.test(s);
}

chrome.contextMenus.onClicked.addListener(function (item, tab) {
  try {
    const decodedText = atob(item.selectionText);
    if (isUrl(decodedText)) {
      chrome.tabs.create({ url: decodedText, index: tab.index + 1 });
    } else {
      alert('This string is not a URL')
    }
  }
  catch (e) {
    alert('This string is not a valid Base64')
  }

});